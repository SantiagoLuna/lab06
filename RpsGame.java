/**
 * @author Santiago Luna
 */

import java.util.Random;
import java.util.Scanner;

public class RpsGame{
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    Random rand = new Random();

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String choice){
        int randomNum = rand.nextInt(3);
        String[] choices = new String[]{"ROCK","SCISSORS","PAPER"};
        String compChoice = choices[randomNum];
        //String turnout = "COMP: "+ compChoice +" ----- "+"USER: " + choice;

        boolean win1 = compChoice.equals(choices[0]) && choice.equals(choices[1]);
        boolean win2 = compChoice.equals(choices[1]) && choice.equals(choices[2]);
        boolean win3 = compChoice.equals(choices[2]) && choice.equals(choices[0]);

        //System.out.println(turnout);
        if(choice.equals(compChoice)){
            //System.out.println("ITS A TIE!");
            this.ties++;
            return "Computer plays " + compChoice + " and you tied!";
        }
        else if(win1 || win2 || win3){
            //System.out.println("You lost!");
            this.losses++;
            return "Computer plays " + compChoice + " and you lost!";
        }
        else if(!win1 && !win2 && !win3){
            //System.out.println("YOU WON");
            this.wins++;
            return "Computer plays " + compChoice + " and you won!";
        }
        return "nothing";
    }
}