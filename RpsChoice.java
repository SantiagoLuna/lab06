/**
 * @author Santiago Luna
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField tf1;
    private TextField tf2;
    private TextField tf3;
    private TextField tf4;
    private String choice;
    private RpsGame game;

    public RpsChoice(TextField tf1,TextField tf2,TextField tf3,TextField tf4, String choice, RpsGame game){
        this.tf1 = tf1;
        this.tf2 = tf2;
        this.tf3 = tf3;
        this.tf4 = tf4;
        this.choice = choice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
        String result = game.playRound(choice.toUpperCase());
        int wins = game.getWins();
        int losses = game.getLosses();
        int ties = game.getTies();

        tf1.setText(result);
        tf2.setText("wins: " + wins);
        tf3.setText("Losses: " + losses);
        tf4.setText("Ties: " + ties);
    }
}
