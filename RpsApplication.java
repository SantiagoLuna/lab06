/**
 * @author Santiago Luna
 */

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{
    private RpsGame game = new RpsGame();

    public void start(Stage stage){
        Group root = new Group();

        //scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 

        HBox buttons = new HBox();
        Button button1 = new Button("Rock");
        Button button2 = new Button("Scissors");
        Button button3 = new Button("Paper");
        buttons.getChildren().addAll(button1,button2,button3);

        HBox textFields = new HBox();
        TextField tf1 = new TextField("WELCOME!");
        tf1.setPrefWidth(250);
        TextField tf2 = new TextField("WINS: 0");
        TextField tf3 = new TextField("LOSSES: 0");
        TextField tf4 = new TextField("TIES: 0");
        textFields.getChildren().addAll(tf1,tf2,tf3,tf4);

        RpsChoice rock = new RpsChoice(tf1, tf2, tf3, tf4, button1.getText(), game);
        RpsChoice scissors= new RpsChoice(tf1, tf2, tf3, tf4, button2.getText(), game);
        RpsChoice paper = new RpsChoice(tf1, tf2, tf3, tf4, button3.getText(), game);

        button1.setOnAction(rock);
        button2.setOnAction(scissors);
        button3.setOnAction(paper);

        VBox content = new VBox();
        content.getChildren().addAll(buttons, textFields);
        
        root.getChildren().add(content);
		
		stage.show(); 
    }    

    public static void main(String[] args){
        Application.launch(args);
    }
}
